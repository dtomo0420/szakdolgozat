import dash
import time
import threading
import webbrowser
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from PyQt5.QtWidgets import QMessageBox
from sqlalchemy import create_engine
import dash_bootstrap_components as dbc
from dash import html, dcc, Input, Output, dash_table


def db_queries(table_name):
    disk_engine = create_engine(f'sqlite:///datagov.db')

    df_count_of_departures = pd.read_sql(f"""
        SELECT count(*) AS count_of_departures
        FROM {table_name}
        WHERE ARRDEP LIKE 'DEPARTURE';
    """, disk_engine)
    count_of_departures = df_count_of_departures['count_of_departures'].iloc[0]

    df_count_of_arrivals = pd.read_sql(f"""
        SELECT count(*) AS count_of_arrivals
        FROM {table_name}
        WHERE ARRDEP LIKE 'ARRIVAL';
    """, disk_engine)
    count_of_arrivals = df_count_of_arrivals['count_of_arrivals'].iloc[0]

    count_of_totalflights = count_of_arrivals + count_of_departures

    df_count_of_economy = pd.read_sql(f"""
        SELECT count(*) AS count_of_economy
        FROM {table_name}
        WHERE CLASS LIKE 'ECONOMY';
    """, disk_engine)
    count_of_economy = df_count_of_economy['count_of_economy'].iloc[0]

    df_count_of_business = pd.read_sql(f"""
        SELECT count(*) AS count_of_business
        FROM {table_name}
        WHERE CLASS LIKE 'BUSINESS';
    """, disk_engine)
    count_of_business = df_count_of_business['count_of_business'].iloc[0]

    df_count_of_first = pd.read_sql(f"""
        SELECT count(*) AS count_of_first
        FROM {table_name}
        WHERE CLASS LIKE 'FIRST';
    """, disk_engine)
    count_of_first = df_count_of_first['count_of_first'].iloc[0]

    return count_of_totalflights, count_of_departures, count_of_arrivals, count_of_economy, \
           count_of_business, count_of_first


def db_queries_only_curr(table_name):
    disk_engine = create_engine(f'sqlite:///datagov.db')
    top_three_airlines = pd.read_sql_query(f"""
        SELECT AIRLINE, count(*) AS airlinecounts
        FROM {table_name}
        GROUP BY AIRLINE
        ORDER BY airlinecounts DESC
        LIMIT 3;
    """, disk_engine)

    all_airlines = pd.read_sql(f"""
        SELECT AIRLINE
        FROM {table_name}
        GROUP BY AIRLINE;
    """, disk_engine)

    return top_three_airlines, all_airlines


def open_dash():
    time.sleep(3)
    webbrowser.open('http://127.0.0.1:8050/')


def run_dash(table_name, prev_table_name, dashboardtitle, style):
    top_three_airlines, all_airlines = db_queries_only_curr(table_name)

    count_of_totalflights, count_of_departures, count_of_arrivals, count_of_economy, \
        count_of_business, count_of_first,  = db_queries(table_name)
    ly_count_of_totalflights, ly_count_of_departures, ly_count_of_arrivals, ly_count_of_economy,\
        ly_count_of_business, ly_count_of_first = db_queries(prev_table_name)

    if dashboardtitle == "":
        dashboardtitle = "Dashboard"

    disk_engine = create_engine(f'sqlite:///datagov.db')

    dest_air_ec_bu_fi_all = pd.read_sql(f"""
        SELECT AIRLINE, DESTINATION, ECONOMY, BUSINESS, FIRST, TOTAl
        FROM "{table_name}_snd";
    """, disk_engine)

    our_airports = pd.read_sql("""
            SELECT name, continent, municipality, lon, lat, DESTINATION, coordinates
            FROM "airports";
    """, disk_engine)

    if style == 'Egyszerű':
        app = dash.Dash(external_stylesheets=[dbc.themes.COSMO], )
    elif style == 'Sötét':
        app = dash.Dash(external_stylesheets=[dbc.themes.DARKLY], )
    elif style == 'Színes':
        app = dash.Dash(external_stylesheets=[dbc.themes.QUARTZ], )

    def create_card(header, number, diff_string):
        card = [
            dbc.CardHeader(header),
            dbc.CardBody([
                dcc.Markdown(
                    dangerously_allow_html=True,
                    children=[f"{number} db<br><sub>{diff_string}</sub></br>"]
                )
            ])
        ]
        return card

    def traffic_card(header, number, ly_number):
        diff = number - ly_number
        if diff > 0:
            diff_string = f"Növekedés +{diff} fő"
        elif diff < 0:
            diff_string = f"Csökkenés {diff} fő"
        else:
            diff_string = "A tavalyival megegyező"

        diff_percentage = 100 / ly_number * number
        if 95 < diff_percentage < 105:
            card_style = 'primary'
        elif diff_percentage <= 96:
            card_style = 'danger'
        else:
            card_style = 'success'

        card = (
            dbc.Card(
                create_card(header, number, diff_string),
                color=f'{card_style}',
                style={'text-align': 'center'}, inverse=True
            )
        )
        return card

    navbar = dbc.Navbar(id='navbar', children=[
        html.A(
            dbc.Row([
                dbc.Col(
                    dbc.NavbarBrand(f"{dashboardtitle}",
                                    style={'color': 'black',
                                           'fontSize': '50px',
                                           'padding-left': '50px',
                                           'className': 'dashboardtitle'}
                                    )
                )
            ], align="center"),
            href='/'
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Button(id='button', children="Kapcsolat", color="secondary",
                               href='https://www.bud.hu/')
                )
            ],
            className="ms-auto",
        )
    ])

    body_app = dbc.Container([
        dbc.Row(html.Marquee(f"A legnagyobb forgalmat bonyolító légitársaságok {top_three_airlines['AIRLINE'].iloc[0]},"
                             f"{top_three_airlines['AIRLINE'].iloc[1]}, {top_three_airlines['AIRLINE'].iloc[2]}"),
                style={'fontSize': '25px', 'color': 'red', 'className': 'marquee_text', 'margin-top': '10px',
                       'margin-bottom': '10px', 'fontWeight': 'bold'
                       }
                ),

        dbc.Row([
            dbc.Col([
                dbc.Row([
                    dbc.Col(
                        traffic_card("Összes utas", count_of_totalflights, ly_count_of_totalflights)
                    ),
                    dbc.Col(
                        traffic_card("Induló utas", count_of_departures, ly_count_of_departures)
                    ),
                    dbc.Col(
                        traffic_card("Érkező utasok", count_of_arrivals, ly_count_of_arrivals)
                    )
                ]),

                html.Br(),

                dbc.Row([
                    dbc.Col(
                        traffic_card("Turista osztályon utazók száma", count_of_economy, ly_count_of_economy)
                    ),
                    dbc.Col(
                        traffic_card("Üzleti osztályon utazók száma", count_of_business, ly_count_of_business)
                    ),
                    dbc.Col(
                        traffic_card("Első osztályon utazók száma", count_of_first, ly_count_of_first)
                    )
                ]),
            ]),
        ]),

        html.Br(),

        dbc.Row([
            dbc.Col(
                dbc.Col([
                    html.H2("A Budapest Airport-ról elérhető légitársaságok forgalmi adatainak részletezése"),
                    html.Div(id='dropdown-div', children=[
                        dcc.Dropdown(id='airline', options=[{'label': i, 'value': i} for i in
                                                            np.append(['Összes légitársaság'],
                                                                      all_airlines['AIRLINE'])],
                                     value='Összes légitársaság')
                    ], style={'width': '100%', 'display': 'inline-block'}),

                    html.Br(),

                    dbc.Row([
                        html.Div(id='pie-graphh-title', style={'width': '25%'}),
                        html.Div(id='table-output-title', style={'width': '75%'})
                    ]),
                    dbc.Row([
                        html.Div(id='pie-graph', style={'width': '25%'}),
                        html.Div(id='table-output', style={'width': '75%'})
                    ]),

                    html.Br(),

                    dbc.Row([
                        html.Div(id='destination-graph_title', style={'width': '50%'}),
                        html.Div(id='continents-graph-title', style={'width': '50%'})
                    ]),
                    dbc.Row([
                        html.Div(id='destination-graph', style={'width': '50%'}),
                        html.Div(id='continents-graph', style={'width': '50%'})
                    ])], style={'width': '100%', 'text-align': 'center'
                                }, xs=12, sm=12, md=6, lg=6, xl=6)
            )
        ], style={'width': '100%'})
    ], fluid=True)


    @app.callback([Output(component_id='destination-graph_title', component_property='children'),
                   Output(component_id='table-output-title', component_property='children'),
                   Output(component_id='pie-graphh-title', component_property='children'),
                   Output(component_id='continents-graph-title', component_property='children')],
                  Input(component_id='airline', component_property='value'))
    def titles(airline):
        pie_graphh_title_string = 'Értékesített turista, üzleti és első osztályú jegyek aránya' \
            if airline == 'Összes légitársaság' \
            else f'Értékesített turista, üzleti és első osztályú {airline} jegyek aránya'
        pie_graphh_title = html.H4(pie_graphh_title_string)

        table_output_title_string = 'Különböző légitársaságok azonos úticéljainak összehasonlítása osztályokra bontva' \
            if airline == 'Összes légitársaság' \
            else f'A {airline} légitársaság azonos úticéljainak összehasonlítása osztályokra bontva'
        table_output_title = html.H4(table_output_title_string)

        destination_graph_title_string = 'Budapestről elérhető járatok' \
            if airline == 'Összes légitársaság' \
            else f'Budapestről elérhető {airline} járatok'
        destination_graph_title = html.H4(destination_graph_title_string)

        continents_graph_title_string = ['Budapestről elérhető járatok'], \
                    ['(a színek a kontinenseket, a körök mérete a desztinációk felkapottságát reprezentálja)'] \
            if airline == 'Összes légitársaság' \
            else [f'Budapestről elérhető {airline} járatok',
                  '(a színek a kontinenseket, a körök mérete a desztinációk felkapottságát reprezentálja)']
        continents_graph_title = dbc.Row([html.H4(continents_graph_title_string[0]),
                                          html.H6(continents_graph_title_string[1])])

        return destination_graph_title, table_output_title, pie_graphh_title, continents_graph_title

    @app.callback(Output(component_id='continents-graph', component_property='children'),
                  Input(component_id='airline', component_property='value'))
    def continent_graph(airline):
        if airline != 'Összes légitársaság':
            graph_df_simple = dest_air_ec_bu_fi_all[dest_air_ec_bu_fi_all['AIRLINE'] == airline]\
                .reset_index().drop_duplicates()
        else:
            graph_df_simple = dest_air_ec_bu_fi_all.drop_duplicates()

        graph_df = pd.merge(graph_df_simple, our_airports, left_on='DESTINATION',
                            right_on='DESTINATION', how='left')

        fig = px.scatter_geo(graph_df,
                             lat="lat",
                             lon="lon",
                             color="continent",
                             size="TOTAL",
                             hover_name="municipality",
                             labels={"lat": "Szélességi koordináta",
                                     "lon": "Hosszúsági koordináta",
                                     "continent": "Kontinens",
                                     "TOTAL": "Utasok száma",
                                     "municipality": "Város neve"}
                             )


        fig.update_layout(
            margin={"r": 0, "t": 0, "l": 0, "b": 0},
            coloraxis_colorbar_x=0.15,
            geo=dict(
                    showland=True,
                    landcolor='rgb(243, 243, 243)',
                    countrycolor='rgb(204, 204, 204)',
                    showcountries=True,
                    showocean=True,
                    oceancolor='#C4E2FF'
            )
        )
        return dcc.Graph(figure=fig)

    @app.callback(Output(component_id='pie-graph', component_property='children'),
                  Input(component_id='airline', component_property='value'))
    def pie_graph(airline):
        if airline != 'Összes légitársaság':
            pie_df = pd.read_sql_query(f"""
                SELECT AIRLINE, CLASS, count(*) AS airlinecounts
                FROM {table_name}
                WHERE AIRLINE LIKE '{airline}'
                GROUP BY CLASS
                ORDER BY CLASS;
            """, disk_engine)

            ec_num = 0
            bu_num = 0
            fi_num = 0
            for i in pie_df.values:
                if i[1] == 'ECONOMY':
                    ec_num = i[2]
                if i[1] == 'BUSINESS':
                    bu_num = i[2]
                if i[1] == 'FIRST':
                    fi_num = i[2]

        else:
            ec_num = count_of_economy
            bu_num = count_of_business
            fi_num = count_of_first

        colors = ['bronze', 'silver', 'gold']
        fig = go.Figure(data=[go.Pie(
            labels=['Turista', 'Üzleti', 'Első'], values=[ec_num, bu_num, fi_num])])
        fig.update_traces(hoverinfo='label+percent', textinfo='value', textfont_size=20,
                          marker=dict(colors=colors, line=dict(color='#000000', width=2)))
        fig.update_layout(
            margin={"r": 0, "t": 0, "l": 0, "b": 0}
        )
        return dcc.Graph(figure=fig)

    @app.callback(Output(component_id='destination-graph', component_property='children'),
                  Input(component_id='airline', component_property='value'))
    def airline_graph(airline):
        if airline != 'Összes légitársaság':
            graph_df_simple = dest_air_ec_bu_fi_all[['DESTINATION']][dest_air_ec_bu_fi_all['AIRLINE'] == airline] \
                .reset_index().drop_duplicates()
        else:
            graph_df_simple = dest_air_ec_bu_fi_all['DESTINATION'].drop_duplicates()

        graph_df = pd.merge(graph_df_simple, our_airports, left_on='DESTINATION',
                            right_on='DESTINATION', how='left')

        fig = go.Figure()

        for i in range(len(graph_df)):
            fig.add_trace(
                go.Scattergeo(
                    lon=[19.261093, graph_df['lon'][i]],
                    lat=[47.42976, graph_df['lat'][i]],
                    name=graph_df['municipality'][i],
                    mode='lines',
                    line=dict(width=1, color='red'),
                    hovertemplate=
                        f"<i>Város</i>: {graph_df['municipality'][i]}<br>"
                        f"<i>Reptér</i>: {graph_df['name'][i]}",
                    showlegend=False
                )
            )

        fig.update_layout(
            margin={"r": 0, "t": 10, "l": 0, "b": 0},
            geo=dict(
                    showland=True,
                    landcolor='rgb(243, 243, 243)',
                    countrycolor='rgb(204, 204, 204)',
                    showcountries=True,
                    showocean=True,
                    oceancolor='#C4E2FF'
            )
        )

        return dcc.Graph(figure=fig)

    @app.callback(Output(component_id='table-output', component_property='children'),
                  Input(component_id='airline', component_property='value'))
    def airline_table(airline):
        if airline != 'Összes légitársaság':
            table_df = dest_air_ec_bu_fi_all[dest_air_ec_bu_fi_all['AIRLINE'] == airline]
        else:
            table_df = dest_air_ec_bu_fi_all

        table_df_w_airp = pd.merge(table_df, our_airports, left_on='DESTINATION', right_on='DESTINATION', how='inner')

        table_df_w_airp.rename(columns={
            'AIRLINE': 'Légitársaság',
            'municipality': 'Város',
            'name': 'Repülőtér',
            'ECONOMY': 'Turista o. utas (db)',
            'BUSINESS': 'Üzleti o. utas (db)',
            'FIRST': 'Első o. utas (db)',
            'TOTAL': 'Összes utas (db)'

        }, inplace=True)

        return dash_table.DataTable(
            data=table_df_w_airp[
                ['Légitársaság', 'Város', 'Repülőtér', 'Turista o. utas (db)',
                 'Üzleti o. utas (db)', 'Első o. utas (db)', 'Összes utas (db)']].to_dict(
                'records'),
            columns=[{'id': i, 'name': i} for i in table_df_w_airp[
                ['Légitársaság', 'Város', 'Repülőtér', 'Turista o. utas (db)',
                 'Üzleti o. utas (db)', 'Első o. utas (db)', 'Összes utas (db)']].columns],
            fixed_rows={'headers': True},
            style_table={
                'maxHeight': '450px'
            },
            style_header={
                'backgroundColor': 'rgb(225,225,225)',
                'fontWeight': 'bold',
                'fontSize': '15px'
            },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(240,240,240)',
                    'color': 'black',
                    'fontSize': '12px',
                }, {
                    'if': {'row_index': 'even'},
                    'backgroundColor': 'rgb(255, 255, 255)',
                    'color': 'black',
                    'fontSize': '12px',
                }
            ],
            style_cell={
                'textAlign': 'center',
                'fontFamily': 'Times New Roman',
                'width': '10%',
                'textOverflow': 'ellipsis',
            }
        )

    app.layout = html.Div(id='parent', children=[navbar, body_app])
    app.run_server(debug=False)


def run(sourcedatastring, dashboardtitle, style):
    splits = sourcedatastring.split('.')
    table_name = splits[0]
    expansion = splits[1]
    table_name_splitted = table_name.split('_')

    if table_name_splitted[0] == 'BUDAIRTRAFFIC' and len(table_name_splitted[1]) == 4 and \
            1 <= len(table_name_splitted[2]) <= 2 and len(table_name_splitted) == 3:
        if expansion == 'csv':
            disk_engine = create_engine(f'sqlite:///datagov.db')
            tables = pd.read_sql(f"""
                    SELECT 
                        name
                    FROM 
                        sqlite_master
                    WHERE 
                        type ='table' AND 
                        name NOT LIKE 'sqlite_master';
                """, disk_engine)

            prev_year = int(table_name_splitted[1]) - 1
            prev_table_name = f'{table_name_splitted[0]}_{prev_year}_{table_name_splitted[2]}'

            if prev_table_name in tables.values:
                if table_name not in tables.values:
                    show_popup('Betöltés', 'A dashboard megjelenítését adatelemzés előzi meg.\n'
                                           'Kérem várjon türelemmel. Az előfeldolgozás eltarthat egy darabig.')
                    create_db(table_name)

                try:
                    threading.Thread(target=run_dash, args=(table_name, prev_table_name, dashboardtitle, style),
                                     daemon=True).start()
                    threading.Thread(target=open_dash, daemon=True).start()
                except Exception as err:
                    show_popup(title='Hiba történt!', description='Hiba történt a dashboard előállítása során!')
                    return
            else:
                show_popup('Hiba történt!', 'Szükséges a korábbi év, azonos időszakánakról\n'
                                            'szóló adatállományt feltölteni!')
        else:
            show_popup('Hiba történt!', 'Kérem csv kiterjesztésű fájlt adjon meg!')
    else:
        show_popup('Hiba történt!', 'Nem megfelelő adatállomány. Ezzel a dashboard-dal a Budapest Airport\n'
                                    'forgalmi adatait tudod szemléltetni!')


def show_popup(title, description):
    msg = QMessageBox()
    msg.setWindowTitle(title)
    msg.setText(description)
    msg.setIcon(QMessageBox.Information)
    msg.setStandardButtons(QMessageBox.Ok)
    x = msg.exec_()

def create_db(table_name):
    disk_engine = create_engine(f'sqlite:///datagov.db')

    for df in pd.read_csv(f'SourceData/{table_name}.csv', iterator=True, encoding='utf-8'):
        df['DATE'] = pd.to_datetime(df['DATE'])
        df.to_sql(f'{table_name}', disk_engine)

    snd_query = pd.read_sql(f"""
        SELECT A.AIRLINE, A.DESTINATION,
            (SELECT count(*)
                FROM "{table_name}" AS B
                WHERE A.AIRLINE == B.AIRLINE and A.DESTINATION == B.DESTINATION and B.CLASS == 'ECONOMY'
            ) AS ECONOMY,
            (SELECT count(*)
                FROM "{table_name}" AS B
                WHERE A.AIRLINE == B.AIRLINE and A.DESTINATION == B.DESTINATION and B.CLASS == 'BUSINESS'
            ) AS BUSINESS,
            (SELECT count(*)
                FROM "{table_name}" AS B
                WHERE A.AIRLINE == B.AIRLINE and A.DESTINATION == B.DESTINATION and B.CLASS == 'FIRST'
            ) AS FIRST,
            (SELECT count(*)
                FROM "{table_name}" AS B
                WHERE A.AIRLINE == B.AIRLINE and A.DESTINATION == B.DESTINATION
            ) AS TOTAL
        FROM "{table_name}" AS A
        GROUP BY AIRLINE, DESTINATION;
    """, disk_engine)
    snd_query.to_sql(f"{table_name}_snd", disk_engine)


def create_airport_df():
    disk_engine = create_engine(f'sqlite:///datagov.db')

    table_name = 'airports'
    for df_air in pd.read_csv('https://datahub.io/core/airport-codes/r/airport-codes.csv',
                              iterator=True, encoding='utf-8'):
        df_air[["lon", "lat"]] = df_air["coordinates"].str.split(", ", expand=True)
        df_air.to_sql(f'{table_name}', disk_engine, if_exists='replace')
    pd.read_sql("""
            ALTER TABLE airports RENAME COLUMN iata_code TO DESTINATION;
            """, disk_engine)
